<?php

namespace Jakmall\Recruitment\Calculator\Calculation;

use Illuminate\Contracts\Container\Container;
use Jakmall\Recruitment\Calculator\Container\ContainerServiceProviderInterface;
use Jakmall\Recruitment\Calculator\Calculation\Infrastructure\CalculationManagerInterface;
use Jakmall\Recruitment\Calculator\Calculation\Infrastructure\CalculationService;

class CalculationServiceProvider implements ContainerServiceProviderInterface
{
    /**
     * @inheritDoc
     */
    public function register(Container $container): void
    {
        $container->bind(
            CalculationManagerInterface::class,
            function () {
                return new CalculationService();
            }
        );
    }
}
