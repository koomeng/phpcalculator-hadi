<?php

namespace Jakmall\Recruitment\Calculator\Calculation\Infrastructure;

class CalculationService implements CalculationManagerInterface{

    private $operatorName;

    public function __construct(){
       
    }

    public function setOperatorDesc($operatorName){
        $this->operatorName = strtolower($operatorName);
    }

    public function getOperatorDesc(){
        return $this->operatorName;
    }

    public function getOperator(): string{
        $operatorName = $this->getOperatorDesc();
        $operator = "";
        switch ($operatorName) {
            case "add":
              $operator = "+";
              break;
            case "subtract":
              $operator = "-";
              break;
            case "multiply":
              $operator = "*";
              break;
            case "divide":
                $operator = "/";
                break;
            case "power":
                $operator = "^";
                break;      
            default:
                $operator = "";
        }

        return $operator; 

    }

    public function generateCalculationDescription(array $numbers): string{
        $operator = $this->getOperator();
        $glue = sprintf(' %s ', $operator);

        return implode($glue, $numbers);
    }

    public function getResult(array $numbers): float{
        
        return $this->calculateAll($numbers);
    }

     /**
     * @param array $numbers
     *
     * @return float|int
     */
    private function calculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->calculate($this->calculateAll($numbers), $number);
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    private function calculate($number1, $number2)
    {
        $operatorName = $this->getOperatorDesc();
        $result = 0;
        switch ($operatorName) {
            case "add":
                $result = $number1 + $number2;
                break;
            case "subtract":
                $result = $number1 - $number2;
                break;
            case "multiply":
                $result = $number1 * $number2;
                break;
            case "divide":
                $result = $number1 / $number2;
                break;
            case "power":
                $result = pow($number1, $number2);
                break;      
            default:
                $result = 0;
        }

        return $result;
    }
}
