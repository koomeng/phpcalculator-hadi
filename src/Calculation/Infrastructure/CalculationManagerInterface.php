<?php

namespace Jakmall\Recruitment\Calculator\Calculation\Infrastructure;

interface CalculationManagerInterface
{
    public function setOperatorDesc($operatorName);

    public function getOperatorDesc();

    public function getOperator(): string;

    public function generateCalculationDescription(array $numbers): string;

    public function getResult(array $numbers): float;

}