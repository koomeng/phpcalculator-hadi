<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Support\Facades\App;

class HistoryClearCommand extends HistoryCommand
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        $this->signature = "history:clear {id?}";
        $this->description = "Clear History";
        parent::__construct();
    }

    public function handle(): void
    {
        $id = $this->getInputId();
        $service = $this->getServiceProvider();
        if($id){
            if($service->clear($id)){
                $this->comment("Data with ID $id is removed");
            }else{
                $this->comment("Can't find ID $id to delete");
            }
        }else{
            $service->clearAll();
            $headers = ['ID', 'Command', 'Operation', 'Result'];
            $rows = [];
            $this->table($headers, $rows);
            $this->comment("All history is cleared");
        }
        
    }

}

?>