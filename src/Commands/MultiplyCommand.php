<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Commands\CalculationCommand;

class MultiplyCommand extends CalculationCommand
{
    public function __construct()
    {
        parent::__construct();
    }
    
    protected function getCommandVerb(): string
    {
        return 'multiply';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'multiplied';
    }

}

?>
