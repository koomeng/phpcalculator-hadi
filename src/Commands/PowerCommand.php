<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Commands\CalculationCommand;

class PowerCommand extends CalculationCommand
{
    public function __construct()
    {
        parent::__construct();
    }
    
    protected function getCommandVerb(): string
    {
        return 'power';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'powered';
    }

    protected function validateInput($input): bool{
        if(count($input) > 2){
            $this->comment("This command accepts only two arguments Number");
            exit;
            return false;
        }else{
            return true;
        }
    }

}

?>
