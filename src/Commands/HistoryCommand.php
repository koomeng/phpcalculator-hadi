<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class HistoryCommand extends Command
{

    protected function getInputId()
    {
        return $this->argument('id');
    }

    protected function getDriver()
    {
        return $this->option('driver');
    }

    protected function getServiceProvider(){
        $service =  $this->getLaravel()->make('Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface');
        return $service;
    }

    protected function checkValidDriver($driver){
        $acceptedDriver = ['file','latest', 'composite'];
        if(!in_array($driver,$acceptedDriver)){
            $this->comment("invalid option driver value, driver value must be 'file','latest', or 'composite'");
            exit;
        }
    }

}

?>