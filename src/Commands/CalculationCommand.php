<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class CalculationCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s {numbers* : The numbers to be %s}',
            $commandVerb,
            $this->getCommandPassiveVerb()
        );
        $this->description = sprintf('%s all given Numbers', ucfirst($commandVerb));
        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return '';
    }

    protected function getCommandPassiveVerb(): string
    {
        return '';
    }

    public function handle(): void
    {
        $numbers = $this->getInput();
        if($this->validateInput($numbers)){
            $calcService = $this->getCalculationServiceProvider();
            $calcService->setOperatorDesc($this->getCommandVerb());
            $description = $calcService->generateCalculationDescription($numbers);
            $result = $calcService->getResult($numbers);

            $this->comment(sprintf('%s = %s', $description, $result));

            #save log
            $arrCommand = [
                'command' => ucfirst($this->getCommandVerb()),
                'operation' => $description,
                'result' => $result
            ];
            $this->saveToLog($arrCommand);
        }else{
            $this->comment("Invalid Input");
        }
    }

    protected function getInput(): array
    {
        return $this->argument('numbers');
    }


    protected function getCalculationServiceProvider(){
        $service =  $this->getLaravel()->make('Jakmall\Recruitment\Calculator\Calculation\Infrastructure\CalculationManagerInterface');
        return $service;
    }

    protected function getHistoryServiceProvider(){
        $service =  $this->getLaravel()->make('Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface');
        return $service;
    }

    protected function saveToLog($arrCommand){
        $service = $this->getHistoryServiceProvider();
        $service->log($arrCommand);
    }

    protected function validateInput($input): bool{
        return true;
    }
}

?>