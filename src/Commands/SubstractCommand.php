<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Commands\CalculationCommand;

class SubstractCommand extends CalculationCommand
{
    public function __construct()
    {
        parent::__construct();
    }
    
    protected function getCommandVerb(): string
    {
        return 'subtract';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'subtracted';
    }

}

?>
