<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Support\Facades\App;

class HistoryListCommand extends HistoryCommand
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        $this->signature = "history:list {id?} {--driver=composite}";
        $this->description = "List of History";
        parent::__construct();
    }

    public function handle(): void
    {
        $id = $this->getInputId();
        $driver = $this->getDriver();
        $driver = strtolower($driver);
        #validate driveroption value
        $this->checkValidDriver($driver);

        $service = $this->getServiceProvider();
        $service->setDriver($driver);
        if($id){
            $result = $service->find($id);
        }else{
            $result = $service->findAll();
        }
        $headers = ['ID', 'Command', 'Operation', 'Result'];
        $rows = $result;
        $this->table($headers, $rows);
        // $this->comment("Result all history for ".$result);
    }

}

?>