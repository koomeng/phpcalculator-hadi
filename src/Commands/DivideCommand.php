<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Commands\CalculationCommand;

class DivideCommand extends CalculationCommand
{
    public function __construct()
    {
        parent::__construct();
    }
    
    protected function getCommandVerb(): string
    {
        return 'divide';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'divided';
    }

}

?>
