<?php

namespace Jakmall\Recruitment\Calculator\History\Infrastructure;

use Jakmall\Recruitment\Calculator\History\Infrastructure\Driver\FileDriver;
use Jakmall\Recruitment\Calculator\History\Infrastructure\Driver\LatestDriver;

class CommandHistoryService implements CommandHistoryManagerInterface{


    private $driver;
    private $fileDriver;
    private $latestDriver;

    public function __construct(FileDriver $fileDriver, LatestDriver $latestDriver)
    {
        $this->fileDriver = $fileDriver;
        $this->latestDriver = $latestDriver;
        
    }

    public function findAll(): array{
        $result = [];
        if($this->getDriver() == 'composite' || $this->getDriver() == 'file'){
            $result = $this->fileDriver->findAll();
        }elseif($this->getDriver() == 'latest'){
            $result = $this->latestDriver->findAll();
        }

        return $result;
    }

    public function find($id){
        if($this->getDriver() == 'composite'){
            $result = $this->latestDriver->find($id);
            $result = $result ? $result : $this->fileDriver->find($id);
        }elseif($this->getDriver() == 'latest'){
            $result = $this->latestDriver->find($id);
        }elseif($this->getDriver() == 'file'){
            $result = $this->fileDriver->find($id);
        }

        $result = $result ? [$result] : [];
        if($result){
            $this->latestDriver->insertLog($result[0]); 
        }
        return $result;
    }

    public function log($command): bool{
        $newCommand = $this->fileDriver->insertLog($command);
        $this->latestDriver->insertLog($newCommand);
        return true;
    }

    public function clear($id): bool{
        $deletedFileDriver = $this->fileDriver->clear($id);
        $deletedLatestDriver = $this->latestDriver->clear($id);
        if($deletedFileDriver || $deletedLatestDriver){
            return true;
        }
        return false;
    }

    public function clearAll(): bool{
        return $this->fileDriver->clearAll() && $this->latestDriver->clearAll() ;
    }

    public function setDriver($driver){
        $this->driver = $driver;
    }

    public function getDriver() : string{
        return $this->driver;
    }

}