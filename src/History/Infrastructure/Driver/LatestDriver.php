<?php

namespace Jakmall\Recruitment\Calculator\History\Infrastructure\Driver;


class LatestDriver extends Driver
{

    public function findAll() : array{

        $logPath = $this->getLogPath();
        $logFile = $this->getLatestStorage();
        return $this->readFile($logPath . $logFile);
    }

    public function find($id) : array{
        $result = $this->findAll();
        foreach($result as $res){
            if($res['id'] == $id){
                return $res;
            } 
        }

        return [];
    }

    public function insertLog($command){
        $logs = $this->findAll();

        foreach($logs as $key => $val){
            if($val['id'] == $command['id']  || $command["operation"] == $val["operation"]){
                unset($logs[$key]);
                break;
            }
        }

        if(count($logs) >= 10){
            array_pop($logs);
            
        }
        array_unshift($logs,$command);

        $logPath = $this->getLogPath();
        $logFile = $this->getLatestStorage();
        $jsonData = json_encode($logs);
        file_put_contents($logPath . $logFile, $jsonData);
    }

    public function clearAll() : bool{
        $logPath = $this->getLogPath();
        $logFile = $this->getLatestStorage();
        return $this->clearAllFile($logPath . $logFile);
    }

    public function clear($id): bool{
        $result = $this->findAll();
        $isDeleted = false;
        foreach($result as $key => $val){
            if($val['id'] == $id){
                unset($result[$key]);
                $isDeleted = true;
                break;
            }
        }

        if($isDeleted){
            $newResult = json_encode($result);
            $logPath = $this->getLogPath();
            $logFile = $this->getLatestStorage();
            file_put_contents($logPath . $logFile, $newResult);
            return true;

        }else{
            return false;
        }
    }    

}
