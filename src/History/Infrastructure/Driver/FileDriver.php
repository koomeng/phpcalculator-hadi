<?php

namespace Jakmall\Recruitment\Calculator\History\Infrastructure\Driver;


class FileDriver extends Driver
{

    public function findAll() : array{

        $logPath = $this->getLogPath();
        $logFile = $this->getFileStorage();
        return $this->readFile($logPath . $logFile);
    }

    public function find($id) : array{
        $result = $this->findAll();
        if(isset($result[$id])){
            return $result[$id];
        }

        return [];
    }

    public function clearAll() : bool{
        $logPath = $this->getLogPath();
        $logFile = $this->getFileStorage();
        return $this->clearAllFile($logPath . $logFile);
    }

    public function clear($id): bool{
        $result = $this->findAll();
        if(isset($result[$id])){
            unset($result[$id]);
            $newResult = json_encode($result);
            $logPath = $this->getLogPath();
            $logFile = $this->getFileStorage();
            file_put_contents($logPath . $logFile, $newResult);
            return true;

        }else{
            return false;
        }
    }

    # return id
    public function insertLog($command) :array {
        $logs = $this->findAll();
        $newId = $logs ? max(array_keys($logs)) + 1 : 1 ;
        $logs[$newId] = [
            'id' => $newId,
            'command' => $command['command'],
            'operation' => $command['operation'],
            'result' => $command['result']
        ];

        $logPath = $this->getLogPath();
        $logFile = $this->getFileStorage();
        $jsonData = json_encode($logs);
        file_put_contents($logPath . $logFile, $jsonData);

        return $logs[$newId];
    }

}
