<?php

namespace Jakmall\Recruitment\Calculator\History\Infrastructure\Driver;


class Driver
{

    protected function getLogPath() : string{
        $logPath = (dirname(__FILE__)."/../../../../log/");
        return $logPath;
    }

    protected function getFileStorage() : string{
        return "mesinhitung.log";
    }

    protected function getLatestStorage() : string{
        return "latest.log";
    }

    protected function readFile($dir) : array {
        $log = file_get_contents($dir);
        $result = json_decode($log, true);
        return $result;
    }

    public function clearAllFile($dir) : bool{
        # replace file;
        $result = json_encode([]);
        file_put_contents($dir, $result);

        return true;
    } 

}
