<?php

namespace Jakmall\Recruitment\Calculator\History;

use Illuminate\Contracts\Container\Container;
use Jakmall\Recruitment\Calculator\Container\ContainerServiceProviderInterface;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryService;
use Jakmall\Recruitment\Calculator\History\Infrastructure\Driver\FileDriver;
use Jakmall\Recruitment\Calculator\History\Infrastructure\Driver\LatestDriver;

class CommandHistoryServiceProvider implements ContainerServiceProviderInterface
{
    /**
     * @inheritDoc
     */
    public function register(Container $container): void
    {
        $container->bind(
            CommandHistoryManagerInterface::class,
            function () {
                //todo: register implementation
                $fileDriver = new FileDriver();
                $latestDriver = new LatestDriver();
                return new CommandHistoryService($fileDriver, $latestDriver);
            }
        );
    }
}
