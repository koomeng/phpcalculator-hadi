<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryController
{
    public function index(Request $request, CommandHistoryManagerInterface $historyService)
    {
        $req = $request->all();
        $driver = isset($req['driver']) ? $req['driver'] : "composite";
        $historyService->setDriver($driver);
        $results = $historyService->findAll();
        return $this->generateResponse($results, $driver);
        
    }

    public function show(Request $request, CommandHistoryManagerInterface $historyService, $id)
    {
        $req = $request->all();
        $driver = isset($req['driver']) ? $req['driver'] : "composite";
        $historyService->setDriver($driver);
        $results = $historyService->find($id);
        return $this->generateResponse($results, $driver, false);
    }

    public function remove(CommandHistoryManagerInterface $historyService, $id)
    {
        $historyService->clear($id);
        return new JsonResponse(null, 204);
    }

    private function generateResponse($results, $driver, $isMulti=true){
        if($driver != "latest"){
            foreach($results as $result){
                $res[] = $result;
            }
        }else{
            $res = $results;     
        }

        $res = $this->generateInput($res);

        $status = 200;
        if(!$res){
            $status = 404;
            $res = [
                "message" => "Data not Found"
            ];
        }else{
            $res = $isMulti ? $res : $res[0];
        }

        return new JsonResponse($res, $status);
    }

    private function generateInput($result){
        foreach($result as &$res){
            $res["command"] = strtolower($res["command"]);
            switch ($res["command"]) {
                case "add":
                  $input = explode(" + ", $res["operation"]);
                  break;
                case "subtract":
                  $input = explode(" - ", $res["operation"]);
                  break;
                case "multiply":
                    $input = explode(" * ", $res["operation"]);
                  break;
                case "divide":
                    $input = explode(" / ", $res["operation"]);
                    break;
                case "power":
                    $input = explode(" ^ ", $res["operation"]);
                    break;
                default:
                    $input = [];               
            }

            $res["input"] = $input;
        }

        return $result;
    }
}
