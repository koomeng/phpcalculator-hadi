<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Jakmall\Recruitment\Calculator\Calculation\Infrastructure\CalculationManagerInterface;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class CalculatorController
{
    public function calculate(Request $request, $action, CalculationManagerInterface $calculationService, CommandHistoryManagerInterface $historyService)
    {
        $req = $request->all();
        $input = isset($req['input']) ? $req['input'] : [];
        $acceptAction = ["add","subtract", "multiply", "divide", "power"];
        $action = strtolower($action);
        if($input && in_array($action, $acceptAction)){
            $calculationService->setOperatorDesc($action);
            $operation = $calculationService->generateCalculationDescription($input);
            $result = $calculationService->getResult($input);

            $res = [
                "command" => ucfirst($action),
                "operation" => $operation,
                "result" => $result
            ];

            $historyService->log($res);

            $status = 200;
        }else{
            $message = "Input Request is Required";
            if(!in_array($action,$acceptAction)){
                $message = "Action must be one of ".implode(",",$acceptAction);
            }
            $status = 500;
            $res = ["message" => $message];
        }
        
       return new JsonResponse($res, $status);
    }

}
